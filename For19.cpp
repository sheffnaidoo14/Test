#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	int N, i, res;
	cout << "N = ";
	cin >> N;
	i = 1;
	res = 1;
	while (i <= N)
	{
		res = res * i;
		i++;
	}
	cout << "res = " << res;
	return 0;
}